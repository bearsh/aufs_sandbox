Aufs and chroot based sandbox
=============================

The goal of this script is to set up a chroot with the content of the own root filesystem where we can write to without modifying the root filesystem itself.
This allows us to e.g. install and try out a software without a risk.  
Please keep in mind that this does not increase security compared to other chroot attempts but allows us to 'reuse' our normal installation.
It may also be slower compared to a traditional chroot.  
Another advantage of this approach ithat we can easily track file modification. So we can for example install a software and afterwards, all modified files will be in the wronly folder.

Filesystem Layout
-----------------
    ${SANDBOX_DIR}/
        -> wronly/   --------> write only, changes between / and ${SANDBOX_DIR}/chroot
            -> bin/    \
            -> etc/    | 1. mount -t aufs -o br:${SANDBOX_DIR}/wronly/[bin,etc,...,var] none ${SANDBOX_DIR}/chroot/[bin,etc,...,var]
               ...     |
            -> var/    /
        -> chroot/   --------> working folder, where we chroot in
            -> bin/    \
            -> etc/    | 2. mount -t -o remount,append:/[bin,etc,...,var] ${SANDBOX_DIR}/chroot/[bin,etc,...,var]
               ...     |
            -> var/    /
    
            -> sys/      3.1 mount --bind /sys ${SANDBOX_DIR}/chroot/sys
            -> proc/     3.2 mount -t proc none ${SANDBOX_DIR}/chroot/proc

TODO
----
- Add options the script
- ...